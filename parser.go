package dragoman

import (
	"strings"
	"os"
	"regexp"

	"errors"
	"fmt"
)

var REGEX = regexp.MustCompile("\\$\\{(?P<name>[a-zA-Z0-9_]+)(?::(?P<default>.*))?\\}")

func interpolateEnvs(data []byte) string {
	dataAsString := string(data)
	matches := REGEX.FindAllStringSubmatch(dataAsString, -1)
	for _, match := range matches {
		original, name, value := extractEnv(match)
		if len(value) == 0 {
			panic(errors.New(fmt.Sprintf("unable to resolve environment variable for \"$%s\"", name)))
		}
		dataAsString = strings.Replace(dataAsString, original, value, 1)
	}
	return dataAsString
}

func extractEnv(match []string) (string, string, string) {
	name := match[1]
	defaultValue := match[2]
	env := getEnv(name)
	if len(env) == 0 {
		return match[0], name, defaultValue
	} else {
		return match[0], name, env
	}
}

func getEnv(env string) (string) {
	return os.Getenv(env)
}
