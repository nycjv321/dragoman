package dragoman

type Options struct {
	Mode, Path string
}

func (options Options) GetMode() (string) {
	mode := options.Mode
	if len(mode) == 0 {
		return YML
	} else {
		return mode
	}
}

func (options Options) GetConfigurationPath() (string) {
	path := options.Path
	if len(path) == 0 {
		return "config.yml"
	} else {
		return path
	}
}