package dragoman

import (
	"testing"
	"os"
)

type Config struct {
	Defaults int `json:"defaults"`
	Required string `json:"required"`
	Provided string `json:"provided"`
	Empty string `json:"empty"`
}

func TestLoadConfiguration(t *testing.T) {
	os.Setenv("REQUIRED", "test")
	config := Config{}
	if err := LoadConfiguration(&config, Options{}); err != nil {
		t.Fatal(err)
	}

	if config.Defaults != 33 {
		t.Fatalf("Config#Defaults was not resolved to \"33\"")
	}

	if config.Required != "test" {
		t.Fatalf("Config#Required was not resolved to \"test\"")
	}

	if config.Provided != "value" {
		t.Fatalf("Config#Provided was not resolved to \"value\"")
	}

	if len(config.Empty) != 0 {
		t.Fatalf("Config#Empty was not empty")
	}
}