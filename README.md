# Dragoman

## About

A _configuration-to-struct_ library for Golang. It supports defining config 
 (e.g. YML) and then unmarshalling it to your own structs. In a world of 
 containers, CI/CD, and "fluid" deployments,  you may want the 
 flexibility of defining config via environment variables. This library 
 supports that as well. See the section titled _Interpolation Format_ below.

This library was written to allow users to centralize application 
configuration to a central struct or a small set of structs.

It was influenced/inspired by [Spring Boot's @ConfigurationProperties annotation](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html).

By default configuration is read from a file located at `./config.yml`. This can 
be overridden via `dragoman.Options`.

### YML Unmarshalling

YML Unmarshalling is done via [https://github.com/go-yaml/yaml](https://github.com/go-yaml/yaml).

## meaning

_A dragoman was an interpreter, translator, and official guide between 
Turkish, Arabic, and Persian-speaking countries and polities of the 
Middle East and European embassies, consulates, vice-consulates and 
trading posts. A dragoman had to have a knowledge of Arabic, Persian, 
Turkish, and European languages. -- from Wikipedia's entry on "Dragoman"_


## Interpolation Format

    key: ${ENVIRONMENT_NAME:default value}

In the above example, the resolved value maps to a struct field labeled "key". 
If the environment variable "$ENVIRONMENT_NAME" is not defined, then "key" is 
resolved to "default value", otherwise it becomes whatever the environment variable 
is set to. 

If "default value" (the value after the ":") is not provided and the 
environment variable is undefined then the library panics.


Please see below and reference the unit tests for concrete examples. 

## Usage

    type MyConfiguration struct {
      Hostname string `json:"hostname"`
      Port int `json:"port"`
      Timeout int `json:"timeout"`
    }

    # in ./config.yml
        hostname: ${HOSTNAME:127.0.0.1}
        port: ${PORT}
        timeout: 100

    # The environment var $PORT is set to 8081
    config := MyConfiguration{}
    if err := dragoman.LoadConfiguration(&config, dragoman.Options{}); err == nil {
      // do stuff with your config
      fmt.Println(config.Hostname) # should print 127.0.0.1
      fmt.Println(config.Port)     # should print 8081
      fmt.Println(config.timeout)  # should print 100
    } else {
        # if we couldn't load the config you should probobly panic (literally as well)
        panic(err)
    }
    
## TODO

In the future, I plan to add the following support:

1. Reading from JSON
2. Reading YML/JSON from a web server.
3. Testing of actual usage.

I do accept pull requests!