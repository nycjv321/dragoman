package dragoman

import (
	"gopkg.in/yaml.v2"
	"errors"
)

const (
	CLOUD      = "cloud"
	YML        = "yml"
)


func LoadConfiguration(configuration interface{}, options Options) (error) {
	switch options.GetMode() {
	case YML:
		return loadYaml(configuration, options)
	}
	return errors.New("configuration was not loaded")
}


func loadYaml(configuration interface{}, options Options) (error) {
	if data, err := readFile(options.GetConfigurationPath()); err == nil {
		if err := yaml.Unmarshal([]byte(interpolateEnvs(data)), configuration); err != nil {
			return err
		} else {
			return nil
		}
	} else {
		return err
	}

}
