package dragoman

import "io/ioutil"

func readFile(path string) ([]byte, error) {
	return ioutil.ReadFile(path) // just pass the file name
}
